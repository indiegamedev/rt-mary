﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace PlayerControls
{
    /// <summary>
    /// Handles joystick funtionality and player input
    /// Implements IPointerDownHandler event system to handle when the player presses the joystick
    /// Implements IPointerUpHandler event system to handle when the player releases the joystick
    /// Implements IDragHandler event system to handle when the player, after pressing the joystick, moves the joystick around
    /// </summary>
    public class Joystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        /// <summary>
        /// Property to get player input on the horizontal axis
        /// </summary>
        public float HorizontalMovement
        {
            get
            {
                return joystickInput.x;
            }
        }

        /// <summary>
        /// Property to get player input on the vertical axis
        /// </summary>
        public float VerticalMovement
        {
            get
            {
                return joystickInput.y;
            }
        }

        /// <summary>
        /// Fields for handling joystick movement and input
        /// </summary>
        public float joystickHandleRange;    // Determines the distance the handle can move from the center position
        public float joystickWiggleRoom;     // Determines the distance the handle can move from the center position before input is registered (how tight the controls are)
        public RectTransform joystickBase;   // The RectTransform of the joystick base' UI
        public RectTransform joystickHandle; // The RectTransform of the joystick handle's UI
        private Canvas joystickCanvas;       // The canvas containing the joystick UI
        private Vector2 joystickCenter;      // The joystick's center position (the joystick's default rest position)
        private Vector2 joystickInput;       // Vecor2 for caching the joystick input from the player

        /// <summary>
        /// Initialize field values and exception handling
        /// </summary>
        private void Start()
        {
            // Cache the joystick UI canvas, then do a null check on the canvas
            joystickCanvas = GetComponentInParent<Canvas>();

            // If joystick is not a child of a canvas...
            if (joystickCanvas == null)
            {
                // Log an error
                Debug.LogError("Joystick is not inside a canvas");
            }

            joystickInput = Vector2.zero;                      // Set joystick's input to zero
            joystickCenter = new Vector2(0.5f, 0.5f);          // Set joystick's defualt rest location (center)
            joystickBase.pivot = joystickCenter;               // Set joystick's pivot point to the default rest location (center)
            joystickHandle.anchorMin = joystickCenter;         // Set joystick handle's minimum anchor point to the default rest location (center)
            joystickHandle.anchorMax = joystickCenter;         // Set joystick handle's maximum anchor point to the default rest location (center)
            joystickHandle.pivot = joystickCenter;             // Set joystick handle's pivot point to eh default rest location (center)
            joystickHandle.anchoredPosition = Vector2.zero;    // Set joystick handle's anchored position to 0,0 so it's at the pivot point
        }

        /// <summary>
        /// Implement IPointerDownHandler method OnPointerDown to handle when the player presses the joystick
        /// </summary>
        /// <param name="eventData">Positional data of where player pressed the joystick</param>
        public void OnPointerDown(PointerEventData eventData)
        {
            // When player presses the joystick, call the OnDrag method to handle moving the joystick around
            OnDrag(eventData);
        }

        /// <summary>
        /// Implement IDragHandler method OnDrag to handle when the player moves the joystick around
        /// </summary>
        /// <param name="eventData">Positional data of where player is dragging joystick</param>
        public void OnDrag(PointerEventData eventData)
        {
            Vector2 BasePosition = joystickBase.position;                                                        // Get joystick base position
            Vector2 BaseRadius = joystickBase.sizeDelta / 2;                                                     // Get joystick base radius
            joystickInput = (eventData.position - BasePosition) / (BaseRadius * joystickCanvas.scaleFactor);     // Determine joystick input by getting the distance from default rest position
            HandleInput(joystickInput.magnitude, joystickInput.normalized);                                      // Call HandleInput method to handle tightness of controls
            joystickHandle.anchoredPosition = joystickInput * BaseRadius * joystickHandleRange;                  // Set joystick handle position
            
        }

        /// <summary>
        /// Handles tightness of controls
        /// </summary>
        /// <param name="magnitude">The distance from handle's default rest location to the handle's current position</param>
        /// <param name="normalized">Sets vector length to 1</param>
        private void HandleInput(float magnitude, Vector2 normalized)
        {
            // If joystick distance from center (magnitude) is greater than the wiggleroom...
            if (magnitude > joystickWiggleRoom)
            {
                // And if magnitude is greater than 1
                if (magnitude > 1)
                {
                    // Normalize joystick input magnitude
                    joystickInput = normalized;
                }
            }
            // Magnitude is less than wiggle room
            else
            {
                // So joystick input is not recognized
                joystickInput = Vector2.zero;
            }
        }

        /// <summary>
        /// Handles when the player releases joystick
        /// </summary>
        /// <param name="eventData">Positional data of where the player released the joystick</param>
        public void OnPointerUp(PointerEventData eventData)
        {
            joystickInput = Vector2.zero;                    // Reset joystick back to center position
            joystickHandle.anchoredPosition = Vector2.zero;  // Reset joystick anchored position back to center
        }

    }
}
