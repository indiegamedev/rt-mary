﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerControls
{
    /// <summary>
    /// This class handles the strafing of the player side to side and forward to back
    /// </summary>
    public class PlayerWalk : MonoBehaviour
    {
        public Joystick joystick;                     // Reference to the Joystick functionality script
        [SerializeField] private float movementSpeed; // player walk speed modifier
        private Vector3 movementDirection;            // Determines movement direction based on joystick input

        /// <summary>
        /// Initializes fields and exception handling
        /// </summary>
        private void Start()
        {
            // Set a default movement speed
            movementSpeed = 3f;

            // Makes sure script is attached to player game object
            // If this game object doesn't isn't tagged as "Player"...
            if (!this.CompareTag("Player"))
            {
                // Log an error
                Debug.LogError("Either the PlayerController script is not attached to the player gameobject or player gameobject is missing the 'Player' tag'");
            }
        }

        /// <summary>
        /// Handles player movement every frame
        /// </summary>
        private void Update()
        {
            // Call PlayerMovement method
            PlayerMovement();
        }

        /// <summary>
        /// Player movement logic
        /// </summary>
        private void PlayerMovement()
        {
            // Determine movement direction based on joystick input from Joystick script
            movementDirection = Vector3.forward * joystick.VerticalMovement + Vector3.right * joystick.HorizontalMovement;

            // Move player according to movementDirection, movement speed, and deltaTime
            transform.Translate(movementDirection * movementSpeed * Time.deltaTime);
        }
    }
}
