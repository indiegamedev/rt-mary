﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlayerControls
{

    /// <summary>
    /// Handles functionality for looking around
    /// </summary>
    public class PlayerLook : MonoBehaviour
    {
        public bool isYAxisInverted;                // Is y axis look inversion enabled or disabled?
        public float lookSensitivity;               // Determines how sensitive the joystick is (how fast the player looks around)
        public Joystick joystick;                   // Reference to the joystick functionality script
        private Transform playerTransform;          // Reference to the player's transform (player view will be a child of the player game object)
        private Quaternion lookDirection;           // Determines the look direction of the player

        /// <summary>
        /// Initialize fields and exception handling
        /// </summary>
        private void Start()
        {
            // If tag check returns false...
            if (!transform.parent.CompareTag("Player"))
            {
                // Log an error
                Debug.LogError("Either this script is not attached to the player gameobject or player gameobject is missing the 'Player' tag'");
            }

            playerTransform = transform.parent.transform; // Cache the parent player game object's transform
            isYAxisInverted = false;                      // Set y axis look inversion to false by default
        }
        
        /// <summary>
        /// Run player view logic every frame
        /// </summary>
        private void Update()
        {
            // Call PlayerView
            PlayerView();
        }

        /// <summary>
        /// Handles player view logic
        /// </summary>
        private void PlayerView()
        {
            // Rotate player view game object around its x axis (look up and down)
            transform.RotateAround(transform.position, playerTransform.right, (isYAxisInverted ? joystick.VerticalMovement : joystick.VerticalMovement * -1) * 360 * lookSensitivity * Time.deltaTime);
            
            // Keep player from looking too far up
            if (transform.eulerAngles.x < 285 && transform.eulerAngles.x > 271)
            {
                transform.localEulerAngles = new Vector3(285, transform.localRotation.y, transform.localRotation.z);
            }

            // Keep player from looking too far down
            if (transform.eulerAngles.x > 75 && transform.eulerAngles.x < 89)
            {
                transform.localEulerAngles = new Vector3(75, transform.localRotation.y, transform.localRotation.z);
            }

            // Rotate parent player object around its y axis (look left and right)
            playerTransform.Rotate(playerTransform.up, joystick.HorizontalMovement * 360 * lookSensitivity * Time.deltaTime);
        }
    }
}
