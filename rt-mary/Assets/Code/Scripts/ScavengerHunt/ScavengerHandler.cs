﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScavengerHandler : MonoBehaviour
{   
    public Inventory inventory = new Inventory();

    [Header("Environment Regions")]
    [SerializeField]
    Transform mainRegions;//First bathroom regions

    [SerializeField]
    Transform dimensionRegions;//Second bathroom regions

    [Header("Spawnable Item Lists")]
    [SerializeField]
    List<Item> questItems;//Items such as syringe

    [SerializeField]
    List<Item> reusableItems;//Items such as corpses or blood packs

    //How many regions are populated
    int regionsPopulated;

    void Start()
    {
        PopulateEnvironment(mainRegions);
    }

    //Randomly spawn items in regions
    void PopulateEnvironment(Transform regions)
    {
        SpawnItems(questItems, regions);
        SpawnItems(reusableItems, regions, true);
        regionsPopulated = 0;
    }

    /*If fillRest is false randomly spawn the quest items
    If fillRest is true, then spawn reusables in the remaining regions*/
    void SpawnItems(List<Item> itemList, Transform regions, bool fillRest = false)
    {
        if (fillRest)
        {
            for (int _ = regionsPopulated; _ < regions.childCount; _++)
            {
                int randomItemIndex = Random.Range(0, itemList.Count);
                Item item = itemList[randomItemIndex];
                InstantiateItemObject(item, regions);
            }
        }
        else
        {
            foreach (Item item in itemList)
            {
                InstantiateItemObject(item, regions);
            }
        }
    }
    
    //Return a random region that is not populated with an item
    Transform GetRandomRegion(Transform regions)
    {
        Transform randomRegion = null;

        while (randomRegion == null)
        {
            int randomChildIndex = Random.Range(0, regions.childCount);
            Region region = regions.GetChild(randomChildIndex).GetComponent<Region>();
            bool populated = region.populated;
            if (populated) { continue; }
            randomRegion = region.transform;
            region.populated = true;
            regionsPopulated++;
        }
        return randomRegion;
    }

    //Add an ItemData monobehaviour to allow for quick Item referencing
    void AddItemData(GameObject itemObject, Item item)
    {
        ItemData itemData = itemObject.AddComponent<ItemData>();
        itemData.item = item;
    }

    //Object selected callback function for object selection
    void SelectObject(GameObject obj)
    {
        Item itemToAdd = obj.GetComponent<ItemData>().item;
        inventory.AddItem(itemToAdd);
        Debug.Log(itemToAdd.name + " has been picked up!");
        Destroy(obj);
    }

    //Add the item game object into the scene at a random region
    void InstantiateItemObject(Item item, Transform regions)
    {
        Transform randomRegion = GetRandomRegion(regions);
        Transform newItemTransform = Instantiate(item.itemObject).transform;
        AddItemData(newItemTransform.gameObject, item);
        newItemTransform.position = randomRegion.position + new Vector3(0, newItemTransform.localScale.y / 2, 0);
        newItemTransform.GetComponent<ReticleSelectable3DObject>().ReticleSelectCallback += SelectObject;
    }
}
