﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BloodAndTime
{
    [CreateAssetMenu]
    public class BloodSupply : ScriptableObject, ISerializationCallbackReceiver
    {
        [Tooltip("The maximum capacity of the blood supply")]
        public int MaxBloodCount;

        [Tooltip("The current blood supply bloodcount")]
        public int BloodCount;

        // Reset bloodcount at the end of the run
        public void OnAfterDeserialize()
        {
            BloodCount = MaxBloodCount;
        }

        // Won't use but required for ISerializationCallbackReceiver interface
        public void OnBeforeSerialize() { }

        /// <summary>
        /// Increase bloodcount by given amount
        /// </summary>
        /// <param name="amountToIncrease">How much to increase bloodcount by</param>
        public void IncreaseBloodCount(int amountToIncrease)
        {
            if (!(BloodCount >= MaxBloodCount))
            {
                BloodCount += amountToIncrease;
                Debug.Log("Blood supply increased by " + amountToIncrease.ToString() + " blood point(s)");
            }
            else
            {
                Debug.Log("Blood bar is already full");
            }
        }

        /// <summary>
        /// Decrease bloodcount by given amount
        /// </summary>
        /// <param name="amountToDecrease">How much to decrease bloodcount by</param>
        public void DecreaseBloodCount(int amountToDecrease)
        {
            BloodCount -= amountToDecrease;
        }
    }
}
