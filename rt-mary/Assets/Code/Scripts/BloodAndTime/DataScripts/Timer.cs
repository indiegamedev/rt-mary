﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BloodAndTime
{
    [CreateAssetMenu]
    public class Timer : ScriptableObject, ISerializationCallbackReceiver
    {
        [Tooltip("The maximum possible time remaining")]
        public float MaxTime;

        [Tooltip("The current time remaining")]
        public float CurrentTime;

        // Reset timer when run has ended
        public void OnAfterDeserialize()
        {
            CurrentTime = MaxTime;
        }

        // Won't be used but required for ISerializationCallbackReceiver interface
        public void OnBeforeSerialize() { }

        /// <summary>
        /// Increase time by given amount
        /// </summary>
        /// <param name="amountToIncrease">How much to increase time by</param>
        public void IncreaseTime(float amountToIncrease)
        {
            CurrentTime += amountToIncrease;

            if (CurrentTime > MaxTime)
            {
                CurrentTime = MaxTime;
            }
        }

        /// <summary>
        /// Decrease time by given amount
        /// </summary>
        /// <param name="amountToDecrease">How much to decrease time by</param>
        public void DecreaseTime(float amountToDecrease)
        {
            CurrentTime -= amountToDecrease;
        }

        // Reset timer back to full
        public void ResetTimer()
        {
            CurrentTime = MaxTime;
            Debug.Log("Reset timer to full");
        }

        // Deplete timer to 0
        public void DepleteTimer()
        {
            CurrentTime = -0.1f;
            Debug.Log("Depleted timer");
        }
    }
}
