﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BloodAndTime
{   
    /// <summary>
    /// Handles the countdown of time and time bar UI
    /// </summary>
    public class TimeCountdown : MonoBehaviour
    {
        [Tooltip("Reference to the text that displays the remaining time")]
        public Text timerText;

        [Tooltip("Reference to the timer ScriptableObject")]
        public Timer timer;

        // Reference to timer bar UI
        private Image timeBar;

        private void Start()
        {
            timeBar = GetComponent<Image>();
        }

        private void Update()
        {
            Countdown();
        }

        // Countdown by 1 second per second
        public void Countdown()
        {
            if (timer.CurrentTime > 0f)
            {
                timer.DecreaseTime(Time.deltaTime);
                UpdateTImeBarUI();

                // Make sure time remaining can't exceed the max
                if (timer.CurrentTime > timer.MaxTime)
                {
                    timer.CurrentTime = timer.MaxTime;
                }
            }
            else if (timer.CurrentTime < 0f)
            {
                timer.CurrentTime = 0f;
                UpdateTImeBarUI();
                Debug.Log("Time's Up");
            }
        }

        // Update timer bar UI
        public void UpdateTImeBarUI()
        {
            timeBar.fillAmount = timer.CurrentTime / timer.MaxTime;
            timerText.text = timer.CurrentTime.ToString("00.0");
        }
    }
}

