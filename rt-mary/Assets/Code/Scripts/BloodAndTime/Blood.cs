﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BloodAndTime
{
    public class Blood : MonoBehaviour
    {
        [Tooltip("Reference to the text that displays the blood supply")]
        public Text bloodSupplyText;

        [Tooltip("Reference to BloodSupply ScriptableObject")]
        public BloodSupply bloodSupply;

        [Tooltip("Reference to Timer ScriptableObject")]
        public Timer timer;

        [Tooltip("Modifier to determine how much time is added per blood point spilled")]
        public int timeBonus;

        // Reference to blood bar UI
        private Image bloodBar;

        private void Start()
        {
            bloodBar = GetComponent<Image>();
            UpdateBloodUI();
        }

        public void SpillBlood()
        {
            if (!(bloodSupply.BloodCount <= 0))
            {
                bloodSupply.DecreaseBloodCount(1);
                timer.CurrentTime += timeBonus;
                UpdateBloodUI();
                Debug.Log("Blood supply decreased by one and " + timeBonus.ToString() + " seconds was added to the timer");
            }
            else
            {
                Debug.Log("Blood supply is depleted");
            }

        }

        // Update bloodbar UI
        public void UpdateBloodUI()
        {
            if (bloodSupply.BloodCount > 0)
            {
                bloodBar.fillAmount = bloodSupply.BloodCount / (float)bloodSupply.MaxBloodCount;
            }
            else
            {
                bloodBar.fillAmount = 0f;
            }

            bloodSupplyText.text = bloodSupply.BloodCount.ToString();
        }

        // Reset the blood supply to maximum
        public void ResetBloodSupply()
        {
            bloodSupply.BloodCount = bloodSupply.MaxBloodCount;
            UpdateBloodUI();

            Debug.Log("Refilled blood supply");
        }

        // Depletes blood supply to 0
        public void DepleteBloodySupply()
        {
            bloodSupply.BloodCount = 0;
            UpdateBloodUI();

            Debug.Log("Depleted blood supply");
        }
    }
}
