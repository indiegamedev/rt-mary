﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script that casts a cone collider through the player camera reticle and interacts with selectable objects
/// </summary>
public class ReticleConecast : MonoBehaviour
{
	#region Public Fields and Properties

	// This must be set to the scene's Main Camera in the Inspector to work
	[Tooltip("This must be set to this scene's Main Camera to work.")]
	public Camera MainCamera;

	// The following properties were made to work in conjunction with realtime UI. Every time a user modifies a
	// conecast value via slider, these properties guarantee that OnReticleValueChange is called:

	/// <summary>
	/// Weight of interpolation between the screen's height and width in calculations. A value of 0 equates
	/// to using only screen height in calculations, while 1 equates to using just width.
	/// </summary>
	public float ScreenHeightToWidthRatioWeight
	{
		get
		{
			return screenHeightToWidthRatioWeight;
		}
		set
		{
			screenHeightToWidthRatioWeight = value;
			OnReticleConeValueChange();
		}
	}

	/// <summary>
	/// The radius of the cone, in terms of what percentage of the screen's height or width it
	/// occupies from the player's perspective (value between 0 and 1).
	/// </summary>
	public float RadiusAsPercentageOfScreen
	{
		get
		{
			return radiusAsPercentageOfScreen;
		}
		set
		{
			radiusAsPercentageOfScreen = value;
			OnReticleConeValueChange();
		}
	}

	/// <summary>
	/// The maximum distance away that an object can be and still be interactable with this cone.
	/// </summary>
	public float MaximumInteractableDistance
	{
		get
		{
			return maximumInteractableDistance;
		}
		set
		{
			maximumInteractableDistance = value;
			OnReticleConeValueChange();
		}
	}

	#endregion

	#region Private Fields

	// Private variables used to store values passed in via respective properties
	private float screenHeightToWidthRatioWeight;
	private float radiusAsPercentageOfScreen;
	private float maximumInteractableDistance;

	// Tracks the GameObjects that the player has highlighted or selected
	GameObject objectInReticle;
	GameObject selectedObject;

	// Stores all the objects that are currently within the reticle selection cone
	List<GameObject> ObjectsInReticleCone = new List<GameObject>();

	#endregion

	#region MonoBehaviour Inherited Methods

	// Initialization
	void Start()
	{
		objectInReticle = null;
		selectedObject = null;

		// Finds UI in the scene that can register taps and subscribes a callback to it
		TapRegisteringUI tappableUI = FindObjectOfType<TapRegisteringUI>();
		if (tappableUI != null)
		{
			tappableUI.RaiseTapEvent += OnSelectCallback;
		}

		// Find manager scripts in the scene and assign callback methods to their event handlers here
	}

	// Update is called once per frame
	void Update()
	{
		// If there's at least one object in the cone, attempts to find and choose the closest one to
		// this object to use for highlighting or selection purposes.
		if (ObjectsInReticleCone.Count > 0)
		{
			GameObject closestSelectableObject = ObjectsInReticleCone[0];

			// Iterates through the list of objects in the reticle selection cone to find the closest one
			foreach (GameObject tmpObj in ObjectsInReticleCone)
			{
				if ((tmpObj.transform.position - transform.position).sqrMagnitude
					< (closestSelectableObject.transform.position - transform.position).sqrMagnitude)
				{
					closestSelectableObject = tmpObj;
				}
			}

			// If there was no prior tracked object in the reticle, the reticle is hovering over a new object.
			if (objectInReticle == null)
			{
				objectInReticle = closestSelectableObject;
				objectInReticle.SendMessage("OnReticleEnter");
			}
			// If the current closest object in the reticle is different from the closest object last frame,
			// the reticle is highlighting a new object and has stopped highlighting the old object.
			else if (objectInReticle != closestSelectableObject)
			{
				objectInReticle.SendMessage("OnReticleExit");
				objectInReticle = closestSelectableObject;
				objectInReticle.SendMessage("OnReticleEnter");
			}
		}
		
		// If the conecast does not contain any objects but it did last frame, then the reticle has stopped
		// hovering over that object.
		else if (objectInReticle != null)
		{
			objectInReticle.SendMessage("OnReticleExit");
			objectInReticle = null;
		}
	}

	#endregion

	#region Public Subscribing Methods

	/// <summary>
	/// Method that is called when the player makes an input to select something
	/// </summary>
	/// <param name="sender">The object that calls this method (usually an input manager)</param>
	/// <param name="e">Event arguments that can be pass info about the selection event</param>
	public void OnSelectCallback(object sender, EventArgs e)
	{
		/* "sender" and "e" are not used in this method, but are necessary in the signature of
		   the method so that the method itself can be assigned as a subscriber to an input manager's
		   EventHandler for when the player chooses to "select" something. */

		if (objectInReticle != null)
		{
			// If the player camera is looking at a selectable object and no object was previously
			// selected, "select" that object and track it.
			if (selectedObject == null)
			{
				selectedObject = objectInReticle;
				objectInReticle.SendMessage("OnReticleSelect");
			}
			// If the player camera is looking at a selectable object that is different from an object
			// that was previously selected, "deselect" the previous object and "select" the new one.
			else if (selectedObject != objectInReticle)
			{
				selectedObject.SendMessage("OnReticleDeselect");
				selectedObject = objectInReticle;
				objectInReticle.SendMessage("OnReticleSelect");
			}
		}
		// If the player camera isn't looking at a selectable object but an object was previously
		// selected, treat the selection input as a "deselection" input.
		else if (selectedObject != null)
		{
			selectedObject.SendMessage("OnReticleDeselect");
			selectedObject = null;
		}
	}

	/// <summary>
	/// Method that is called when a GameObject is removed from the scene, to avoid null references
	/// </summary>
	/// <param name="sender">The object that calls this method (usually the game manager)</param>
	/// <param name="gameobjectBeingRemoved">The GameObject that is going to be removed from the scene</param>
	public void OnInteractableObjectRemoval(object sender, GameObject gameobjectBeingRemoved)
	{
		// If this method is tracking the GameObject that is going to be removed, stops tracking it
		if (selectedObject == gameobjectBeingRemoved)
		{
			selectedObject = null;
		}
		if (objectInReticle == gameobjectBeingRemoved)
		{
			objectInReticle = null;
		}
		if (ObjectsInReticleCone.Contains(gameobjectBeingRemoved))
		{
			ObjectsInReticleCone.Remove(gameobjectBeingRemoved);
		}
	}

	#endregion

	#region Private Methods

	/// <summary>
	/// Updates the dimensions of the reticle selection cone (assumed to be this object) every time
	/// a value related to the cone is changed.
	/// </summary>
	private void OnReticleConeValueChange()
	{
		// Calculates the cone's new radial scale based on the dimensions of the screen, the height-width
		// ratio weight, and maximum distance of the cone.
		float nearClipPlaneHeight = Mathf.Tan(MainCamera.fieldOfView * Mathf.Deg2Rad * 0.5f);
		float nearClipPlaneWidth = nearClipPlaneHeight * MainCamera.aspect;
		float radialScale = Mathf.Lerp(nearClipPlaneHeight, nearClipPlaneWidth, screenHeightToWidthRatioWeight)
			* radiusAsPercentageOfScreen
			* maximumInteractableDistance;

		transform.localScale = new Vector3(radialScale, radialScale, maximumInteractableDistance);
	}

	/// <summary>
	/// Method that is called on the frame that a new object collides with this one.
	/// </summary>
	/// <param name="other">The other collider that is colliding with this object.</param>
	private void OnTriggerEnter(Collider other)
	{
		// Adds the colliding object to the list of objects within the reticle selection cone.
		if (!ObjectsInReticleCone.Contains(other.gameObject))
		{
			ObjectsInReticleCone.Add(other.gameObject);
		}
	}

	/// <summary>
	/// Method that is called on the frame that an object stops colliding with this one.
	/// </summary>
	/// <param name="other">The other collider that is no longer colliding with this object.</param>
	private void OnTriggerExit(Collider other)
	{
		// Removes the previously colliding object from the list of objects within the cone.
		if (ObjectsInReticleCone.Contains(other.gameObject))
		{
			ObjectsInReticleCone.Remove(other.gameObject);
		}
	}

	#endregion
}
