﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace JsonWrapper
{
    /// <summary>
    /// Class that stores credit json data
    /// </summary>
    [Serializable]
    public class CreditsJson
    {
        public List<CreditsSection> sections = new List<CreditsSection>();
    }

    [Serializable]
    public class CreditsSection
    {
        public string title;
        public List<Credit> members = new List<Credit>();
    }

    [Serializable]
    public class Credit
    {
        public string name;
        public string position;
    }
}
