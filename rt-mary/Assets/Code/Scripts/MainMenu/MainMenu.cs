﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using JsonWrapper;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    //Path to the credits json
    //the JSON folder will also store options data
    string creditsJsonPath = "/Code/Scripts/MainMenu/Json/credits.json";
    public GameObject mainMenu;
    public GameObject optionsMenu;
    public GameObject creditsMenu;

    //Credit text
    public Text creditPrefab;

    //Have the credits been loaded yet?
    bool creditsLoaded;

    //Load a scene
    public void LoadScene(int scene)
    {
        SceneManager.LoadScene(scene);
    }

    //Open the options menu
    public void OpenOptions()
    {
        optionsMenu.SetActive(true);
        mainMenu.SetActive(false);
    }

    //Open the main menu
    public void OpenMainMenu()
    {
        optionsMenu.SetActive(false);
        creditsMenu.SetActive(false);
        mainMenu.SetActive(true);
    }

    //Find the body of the credits display and populate it based on the credits json
    public void OpenCredits()
    {
        Transform creditsBody = creditsMenu.transform.Find("Body");
        if (!creditsLoaded)
        {
            string credits = File.ReadAllText(Application.dataPath + creditsJsonPath);
            CreditsJson creditsJson = JsonUtility.FromJson<CreditsJson>(credits);
            Debug.Log(creditsJson.sections.Count);

            foreach(CreditsSection section in creditsJson.sections)
            {
                Debug.Log(section.title);
                Text sectionTitleText = Instantiate(creditPrefab, creditsBody);
                sectionTitleText.resizeTextForBestFit = true;
                sectionTitleText.text = section.title;

                foreach(Credit credit in section.members)
                {
                    Text creditText = Instantiate(creditPrefab, creditsBody);
                    if (credit.name == null) continue;
                    creditText.text = credit.name + " - " + credit.position;
                }
            }
            creditsLoaded = true;
        }
        mainMenu.SetActive(false);
        creditsMenu.SetActive(true);
    }
}
