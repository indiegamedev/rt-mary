﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeHover : MonoBehaviour
{
    float yMin;
    float yMax;
    int dir;
    Vector3 rot;

    void Start()
    {
        rot = new Vector3(Random.Range(0, 0.075f), Random.Range(0, 0.075f), Random.Range(0, 0.075f));
        yMin = Random.Range(1, 2);
        yMax = Random.Range(5, 7);

        dir = Random.Range(0, 2) == 0 ? 1 : -1;
        transform.position = new Vector3(transform.position.x, Random.Range(0,2) == 0 ? yMin : yMax, transform.position.z);
    }

    void Update()
    {
        float t = dir == 1 ? (yMax - transform.position.y)/(yMax - yMin) : (transform.position.y - yMin) / (yMax- yMin);
        Vector3 newPos = Vector3.Lerp(transform.position, transform.position  + new Vector3(0, dir * 0.01f, 0),t * 5);
        transform.position = newPos;
        if(yMax - transform.position.y <= 0.1f)
        {
            dir = -1;
        }
        else if(transform.position.y - yMin <= 0.1f)
        {
            dir = 1;
        }

        transform.Rotate(rot, 0.21f);
    }
}
