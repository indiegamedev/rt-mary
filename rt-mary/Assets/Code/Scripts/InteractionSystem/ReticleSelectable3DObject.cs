﻿using UnityEngine;

/// <summary>
/// Script that defines certain behaviors for reticle selectable 3D objects
/// </summary>
public class ReticleSelectable3DObject : BaseReticleSelectable
{
	// Shader used to highlight this object when in the player camera reticle
	[Tooltip("Shader used to highlight this object when in the player camera reticle")]
	public Shader HighlightShader;

	// Shader used on this object when not in the player camera reticle
	[Tooltip("Shader used on this object when not in the player camera reticle")]
	public Shader StandardShader;
	
	/// <summary>
	/// Override method that changes this object's material shader to the HighlightShader
	/// </summary>
	public override void OnReticleEnter()
	{
		gameObject.GetComponent<MeshRenderer>().material.shader = HighlightShader;

		base.OnReticleEnter();
	}

	/// <summary>
	/// Override method that changes this object's material shader to the StandardShader
	/// </summary>
	public override void OnReticleExit()
	{
		gameObject.GetComponent<MeshRenderer>().material.shader = StandardShader;

		base.OnReticleExit();
	}
}
