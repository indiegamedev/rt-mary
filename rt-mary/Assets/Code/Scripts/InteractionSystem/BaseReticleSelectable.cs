﻿using UnityEngine;

/// <summary>
/// Interface for making objects selectable when in a reticle, as opposed to a pointer
/// </summary>
public interface IReticleSelectHandler
{
	void OnReticleSelect();
	void OnReticleDeselect();
	void OnReticleEnter();
	void OnReticleExit();
}

/// <summary>
/// The base class for objects that can be selected via a camera reticle.
/// </summary>
public class BaseReticleSelectable : MonoBehaviour, IReticleSelectHandler
{
	/* Do not use this class to make objects selectable via reticle!
	   Use a child class so that you can have specific behaviors upon certain events,
	   like making an object change its material shader when highlighted. */

	#region Fields

	// Defines delegate signature for callback events
	public delegate void ReticleEventCallback(GameObject sender);

	/* These are callbacks to the Game Manager that let it know that this object has been selected.
	   When the Game Manager creates an object that can be selected via a camera reticle, it can
	   assign callbacks to these events that update it about this object's status. */
	public ReticleEventCallback ReticleEnterCallback;
	public ReticleEventCallback ReticleExitCallback;
	public ReticleEventCallback ReticleSelectCallback;
	public ReticleEventCallback ReticleDeselectCallback;

	public bool Interactable { get; set; }          // Is this object selectable at the moment?
	public bool Selected { get; protected set; }    // Has this object been selected? 
	public bool Highlighted { get; protected set; } // Is a reticle hovering over this object right now?

	#endregion

	#region MonoBehaviour Inherited Methods

	// Start is called before the first frame update
	void Start()
	{
		Interactable = true;
		Selected = false;
		Highlighted = false;
	}

	// Update is called once per frame
	void Update()
	{

	}

	#endregion

	#region Public Methods

	// For all of the following methods, a callback to the Game Manager is only invoked
	// when a callback is assigned, and will only occur if the object is currently Interactable.

	/// <summary>
	/// Method that is called when this object is hovered over by the camera reticle ("highlighted")
	/// </summary>
	public virtual void OnReticleEnter()
	{
		if (Interactable)
		{
			Highlighted = true;
			ReticleEnterCallback?.Invoke(gameObject);

			Debug.Log(gameObject.name + " highlighted");
		}
	}

	/// <summary>
	/// Method that is called when the camera reticle is no longer hovering over this object
	/// </summary>
	public virtual void OnReticleExit()
	{
		if (Interactable)
		{
			Highlighted = false;
			ReticleExitCallback?.Invoke(gameObject);

			Debug.Log(gameObject.name + " unhighlighted");
		}
	}

	/// <summary>
	/// Method that is called when this object is selected
	/// </summary>
	public virtual void OnReticleSelect()
	{
		if (Interactable)
		{
			Selected = true;
			ReticleSelectCallback?.Invoke(gameObject);

			Debug.Log(gameObject.name + " selected");
		}
	}

	/// <summary>
	/// Method that is called when this object is deselected
	/// </summary>
	public virtual void OnReticleDeselect()
	{
		if (Interactable)
		{
			Selected = false;
			ReticleDeselectCallback?.Invoke(gameObject);

			Debug.Log(gameObject.name + " deselected");
		}
	}

	#endregion
}
