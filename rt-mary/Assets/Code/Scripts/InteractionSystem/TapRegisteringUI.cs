﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Tracks the status of a potential tap
/// </summary>
public class TapTracker
{
	public int TouchID { get; protected set; }              // ID used to find this touch again in the EventSystem
	public Vector2 StartingPosition { get; protected set; } // Position in pixel coordinates where this touch started
	public float TimeActive { get; set; }                   // Amount of time in seconds this touch has existed

	/// <summary>
	/// Creates a new tracker for a potential tap input
	/// </summary>
	/// <param name="touchID">The ID Unity assigned to this touch</param>
	/// <param name="startingPosition">The starting position of this touch, in pixel coordinates</param>
	public TapTracker(int touchID, Vector2 startingPosition)
	{
		TouchID = touchID;
		StartingPosition = startingPosition;
		TimeActive = 0f;
	}
}

/// <summary>
/// Script that lets a UI element recognize when the user taps on it
/// </summary>
public class TapRegisteringUI : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
	#region Fields

	// The maximum amount of time in seconds that a touch can exist before it can no longer be considered a tapping input
	[Range(0f, 30f)]
	[Tooltip("The maximum amount of time in seconds that a touch can exist before it can no longer be considered a tapping input")]
	public float MaximumDurationOfTaps = 0.2f;

	// The radius of the deadzone, in pixels, wherein a touch can be considered a tapping input
	[Range(0f, 500f)]
	[Tooltip("The radius of the deadzone, in pixels, wherein a touch can be considered a tapping input")]
	public float TapDeadzoneThreshold = 5f;

	// Event Handler that lets subscribers know when a "tap" input occurs
	public EventHandler RaiseTapEvent;

	// List that contains all of the active touches that could be potential tap inputs
	List<TapTracker> activeTaps;

	#endregion

	#region MonoBehaviour Inherited Methods

	// Initialization
	void Start()
	{
		activeTaps = new List<TapTracker>();
	}

	// Updates the amount of time each tracked touch on the screen has been active since the last frame
	void Update()
	{
		foreach (TapTracker tap in activeTaps)
		{
			tap.TimeActive += Time.deltaTime;
		}
	}

	#endregion

	#region IPointer Implementations

	/// <summary>
	/// Handles when a pointer presses down on this UI element
	/// </summary>
	/// <param name="eventData">The properties of the pointer at the time of the press</param>
	public void OnPointerDown(PointerEventData eventData)
	{
		// Begins tracking the new touch to see if it will become a tap input
		activeTaps.Add(new TapTracker(eventData.pointerId, eventData.position));
	}

	/// <summary>
	/// Handles when a pointer is no longer pointing at this UI element
	/// </summary>
	/// <param name="eventData">The properties of the pointer at the time it exits</param>
	public void OnPointerExit(PointerEventData eventData)
	{
		// The touch is now out of bounds, so it can no longer be a tap input and should no longer be tracked
		TapTracker tmpTracker = activeTaps.Find(x => x.TouchID == eventData.pointerId);
		if (tmpTracker != null)
		{
			activeTaps.Remove(tmpTracker);
		}
	}

	/// <summary>
	/// Handles when a pointer releases a press on this UI element
	/// </summary>
	/// <param name="eventData">The properties of the pointer at the time of the press's release</param>
	public void OnPointerUp(PointerEventData eventData)
	{
		// If the touch was quick enough and did not move too far away from where it started,
		// then it is considered a tap input, and a Tap Event is raised.
		TapTracker tmpTracker = activeTaps.Find(x => x.TouchID == eventData.pointerId);
		if (tmpTracker != null)
		{
			if (tmpTracker.TimeActive <= MaximumDurationOfTaps &&
				(tmpTracker.StartingPosition - eventData.position).magnitude <= TapDeadzoneThreshold)
			{
				RaiseTapEvent?.Invoke(gameObject, null);
			}

			activeTaps.Remove(tmpTracker);
		}
	}

	#endregion
}
