﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMirrorController : MonoBehaviour
{
    public Transform mirror_1;//first bathroom mirror
    public Transform mirror_2;//second bathroom mirror
    public Transform primaryMirror;//the mirror of the current room the player is in
    public Transform secondaryMirror;//the mirror of the room opposite of the player

    Transform mirroredPlayerObject; //object that represents the player's reflection

    //Set the primary/secondary variables and connect the tap select callback
    void Start()
    {
        primaryMirror = mirror_1;
        secondaryMirror = mirror_2;
        mirroredPlayerObject = GameObject.FindGameObjectWithTag("MirroredPlayerObject").transform;
        mirror_1.GetComponent<ReticleSelectable3DObject>().ReticleSelectCallback += TraverseDimension;
        mirror_2.GetComponent<ReticleSelectable3DObject>().ReticleSelectCallback += TraverseDimension;
    }

    void Update()//Mirror the players position to the player reflection transform's position
    {
        Vector3 playerReflectionOffset = Vector3.Reflect(transform.position, Vector3.forward);
        mirroredPlayerObject.position = (playerReflectionOffset + mirror_2.parent.position);
    }

    void TraverseDimension(GameObject obj)//Send the player to the opposite bathroom and reverse the secondary and primary bathroom variable
    {
        Vector3 teleportPos = secondaryMirror.position;
        transform.position = new Vector3(teleportPos.x, transform.position.y, teleportPos.z) + secondaryMirror.up*2;
        transform.Rotate(new Vector3(0, 180, 0));
        Transform primaryMirrorPlaceholder = primaryMirror;
        primaryMirror = secondaryMirror;
        secondaryMirror = primaryMirrorPlaceholder;
    }
}
