﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorTextureSetup : MonoBehaviour
{

    //Each mirror variable corresponds to the room they're in; _1 = first bathroom, _2 = second bathroom
    public Camera mirrorCam_1;
    public Camera mirrorCam_2;

    //Materials on the mirrors
    public Material mirrorMat_1;
    public Material mirrorMat_2;

    void Start()//reset and set the texture's resolution to the screen resolution to get the desired mirror/portal effect.
    {
        if(mirrorCam_1.targetTexture != null)
        {
            mirrorCam_1.targetTexture.Release();
        }
        if(mirrorCam_2.targetTexture != null)
        {
            mirrorCam_2.targetTexture.Release();
        }
        mirrorCam_1.targetTexture = new RenderTexture(Screen.width, Screen.height, 50);
        mirrorCam_2.targetTexture = new RenderTexture(Screen.width, Screen.height, 50);
        mirrorMat_1.mainTexture = mirrorCam_2.targetTexture;
        mirrorMat_2.mainTexture = mirrorCam_1.targetTexture;
    }
}
