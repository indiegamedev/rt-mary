﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorCamera : MonoBehaviour
{
    //Main camera, player, and player reflection transforms
    Transform playerCam;
    Transform player;
    Transform mirroredPlayerObject;

    //The associated mirror, opposite mirror, and the room that the associated mirror is in
    public Transform mirror; //mirror that the camera is parented to
    public Transform secondaryMirror; //the opposite mirror
    public Transform room; //the opposite room

    //Set player variables
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        playerCam = Camera.main.transform;
    }

    //Move and rotate the mirror camera to give the illusion of a window/portal
    void Update()
    {
        Vector3 playerToMirrorOffset = playerCam.position - secondaryMirror.position;
        Vector3 adjustedPosition = mirror.position + playerToMirrorOffset;
        transform.position = new Vector3(adjustedPosition.x, adjustedPosition.y, mirror.position.z - (secondaryMirror.position.z - playerCam.position.z));

        float rotationDifference = Quaternion.Angle(mirror.rotation, secondaryMirror.rotation);
        Quaternion portalRotationDifference = Quaternion.AngleAxis(rotationDifference, Vector3.up);
        Vector3 newCamDirection = playerCam.forward;
        transform.rotation = Quaternion.LookRotation(newCamDirection, Vector3.up);
    }
}
