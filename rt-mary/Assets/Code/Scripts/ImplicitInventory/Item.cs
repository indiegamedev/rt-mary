﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


///<summary>
///Class that stores game item data such as GameObject prefab, and name.
///<para> Used for Quest and Blood items</para>
///</summary>
[CreateAssetMenu(fileName = "New Item", menuName = "Item")]
public class Item : ScriptableObject
{
    public new string name;
    public GameObject itemObject;
}
