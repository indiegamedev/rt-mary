﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The player's inventory.
/// </summary>
public class Inventory
{
    [SerializeField]
    List<Item> items = new List<Item>();

    //Add an Item to the inventory
    public void AddItem(Item item)
    {
        items.Add(item);
    }

    //Check if player has an Item. Returns true if player has an Item; false otherwise
    public bool HasItem(string itemName)
    {
        foreach(Item item in items)
        {
            if (item.name == itemName)
            {
                return true;
            }
        }
        return false;
    }
}
